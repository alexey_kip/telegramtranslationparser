package ru.truebober.telegram.translation_parser

import java.nio.file.Path
import java.util.*

/**
 * Специфичный для конкретного типа файла парсер.
 */
internal interface SpecificParser {

    /**
     * Запустить анализ файла
     */
    fun run(platform: Platform, locale: Locale, version: Int, filepath: Path): Translation

}


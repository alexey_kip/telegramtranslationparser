package ru.truebober.telegram.translation_parser

import java.util.*

/**
 * Перевод для всех строк клиента телеграмма
 */
data class Translation(
    /**
     * Язык перевода
     */
    val locale: Locale,

    /**
     * Версия
     */
    val version: Int,

    /**
     * Платформа, которой принадлежит перевод
     */
    val platform: Platform,

    /**
     * Строки с переводом
     */
    val body: Map<TranslationKey, String>
)

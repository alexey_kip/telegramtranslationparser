package ru.truebober.telegram.translation_parser

import org.slf4j.LoggerFactory
import ru.truebober.telegram.translation_parser.Plurality.Companion.PLURALITY_REGEX
import java.io.FileReader
import java.nio.file.Path
import java.util.*
import kotlin.collections.HashMap

/**
 * Парсер для переводов, сохраненных в файл формата *.strings
 */
internal object StringsSpecificParser : SpecificParser {

    private val log = LoggerFactory.getLogger(StringsSpecificParser::class.java)

    /**
     * Корректный формат строки: "KEY"="VALUE"
     */
    private val STR_REGEX = "^\"([\\w.#\\-:]+)\"\\s?=\\s\"(.+)\";".toRegex()

    override fun run(platform: Platform, locale: Locale, version: Int, filepath: Path): Translation {
        val resultBody = HashMap<TranslationKey, String>()

        FileReader(filepath.toFile()).useLines {
            it
                .filterNot { str -> str.isBlank() }                         //игнорировать пустые строки
                .filterNot { str -> str.trim().startsWith("//") }     //игнорировать комменты
                .filterNot { str -> str.trim().startsWith("#") }      //игнорировать комменты
                .filterNot { str -> str.trim().startsWith("/*") }     //игнорировать комменты
                .map { str -> STR_REGEX.find(str) }                        //поиск в соответствии с регуляркой
                .filter { matchResult -> matchResult != null && matchResult.groupValues.size == 3 }
                .forEach {
                    val strings = it!!.groupValues
                    val name = strings[1]
                    val value = strings[2]
                    val key = getTranslationKey(name)

                    if (log.isDebugEnabled)
                        log.debug("Param name: [$key], param value: [$value]")

                    if (key.plurality != Plurality.skip)
                        resultBody[key] = value
                }
        }

        return Translation(locale, version, platform, resultBody)
    }

    /**
     * Извлечь из названия параметра ключ перевода
     */
    private fun getTranslationKey(name: String): TranslationKey {
        var key: TranslationKey? = null

        //в переводе содержится множественность
        if (PLURALITY_REGEX.matches(name)) {
            val result = PLURALITY_REGEX.find(name)
            if (result != null) {
                val keyName = result.groupValues[1]
                val pluralityName = result.groupValues[2]
                val plurality = Plurality.get(pluralityName)
                key = TranslationKey(plurality, keyName)
            }
        }

            if (key == null) {
            key = TranslationKey(Plurality.none, name)
        }

        return key
    }
}

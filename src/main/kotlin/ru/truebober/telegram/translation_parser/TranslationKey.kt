package ru.truebober.telegram.translation_parser

/**
 * Значение ключа в переводе
 */
data class TranslationKey(val plurality: Plurality, val keyName: String): Comparable<TranslationKey> {
    override fun compareTo(other: TranslationKey) = this.keyName.compareTo(other.keyName)
}

/**
 * Множественность ключа (если <code>Plurality.none</code>, то ключ только один и не имеет множественности)
 */
enum class Plurality(val alias: Set<String>) {
    none(setOf()),
    zero(setOf("0", "zero")),
    one(setOf("1", "one")),
    two(setOf("2", "two")),
    few(setOf("3_10", "few")),
    many(setOf("many")),
    other(setOf("other", "any")),
    skip(setOf("countable"));

    companion object {

        fun get(value: String): Plurality {
            return values().firstOrNull { v -> v.alias.contains(value) } ?: none
        }

        val PLURALITY_REGEX = "^(.+)[_#](0|zero|1|one|2|two|3_10|few|many|other|any|countable)$".toRegex()
    }
}

package ru.truebober.telegram.translation_parser

import org.slf4j.LoggerFactory
import java.nio.file.Path
import java.util.*
import javax.xml.parsers.DocumentBuilderFactory
import kotlin.collections.HashMap

internal object XmlSpecificParser : SpecificParser {

    private val log = LoggerFactory.getLogger(XmlSpecificParser::class.java)

    private const val NAME_ARG = "name"
    private const val STRING_PARAM = "string"
    private val PLURALITY = "(.+)_([a-z]{3,5})".toRegex()

    override fun run(platform: Platform, locale: Locale, version: Int, filepath: Path): Translation {
        val resultBody = HashMap<TranslationKey, String>()

        //открытие документа
        val docBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder()
        val doc = docBuilder.parse(filepath.toFile())

        //поиск строк
        val strings = doc.getElementsByTagName(STRING_PARAM)

        if (log.isDebugEnabled)
            log.debug("${strings.length} strings")

        //поиск ключа и значения каждого параметра
        for (i in 0 until strings.length) {
            val str = strings.item(i)
            val name = str.attributes.getNamedItem(NAME_ARG).nodeValue ?: continue
            val value = str.firstChild.nodeValue ?: continue

            val key = toTranslationKey(name)

            if (log.isDebugEnabled)
                log.debug("Param name: [$key], param value: [$value]")

            resultBody[key] = value
        }

        return Translation(locale, version, platform, resultBody)
    }

    private fun toTranslationKey(key: String): TranslationKey {
        val keyString: String
        val plurality: Plurality?

        if (PLURALITY.containsMatchIn(key)) {
            val match = PLURALITY.find(key)?.groupValues
            if (match != null && match.size == 3) {
                plurality = Plurality.get(match[2])
                keyString = if (plurality == Plurality.none) key else match[1]
            } else {
                throw IllegalStateException()
            }
        } else {
            keyString = key
            plurality = Plurality.none
        }

        return TranslationKey(plurality, keyString)
    }

}

package ru.truebober.telegram.translation_parser

import java.nio.file.Path

/**
 * Основной интерфейс парсера
 */
interface Parser {

    /**
     * Начать парсинг файла по указанному пути
     */
    fun run(path: Path): Translation

}

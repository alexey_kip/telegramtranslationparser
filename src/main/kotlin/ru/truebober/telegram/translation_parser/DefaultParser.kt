package ru.truebober.telegram.translation_parser

import org.slf4j.LoggerFactory
import java.nio.file.Files
import java.nio.file.Path
import java.util.*

/**
 * Дефолтная реализация парсера. Анализирует название файла и передает его на обработку уже в конкретный парсер для
 * конкретного типа файла
 */
class DefaultParser : Parser {

    companion object {
        private val log = LoggerFactory.getLogger(DefaultParser::class.java)

        /**
         * Шаблон имени файла. Пример корректного имени: android_en_v887668.xml
         */
        private val FILENAME_TEMPLATE = "([a-z]{3,10})_([a-z]{2,3})_v(\\d{1,7})\\.([a-z]{3,10})".toRegex()
    }

    override fun run(path: Path): Translation {
        if (log.isDebugEnabled)
            log.debug("Start parsing {}", path)

        //проверка наличия файла
        assert(Files.exists(path)) { "File is not exists: $path" }

        //получение и проверка имени файла
        val filename = path.fileName.toString();
        val correctName = FILENAME_TEMPLATE.containsMatchIn(filename)
        assert(correctName) { "Incorrect filename: $path" }

        //получение параметров файла
        val groups = FILENAME_TEMPLATE.find(filename)!!.groupValues
        val platformName = groups[1]
        val localeName = groups[2]
        val versionString = groups[3]
        val extension = groups[4]

        if (log.isDebugEnabled)
            log.debug("Platform [$platformName], Locale [$localeName], Version [$versionString], Extension [$extension]")

        val version = versionString.toInt()
        val locale = Locale(localeName)
        val platform = Platform.valueOf(platformName)

        //выбор и запуск нужного парсера
        return getInnerParser(extension).run(platform, locale, version, path)
    }

    private fun getInnerParser(extension: String): SpecificParser {
        return when (extension) {
            "xml" -> XmlSpecificParser
            "strings" -> StringsSpecificParser
            else -> throw UnsupportedOperationException(extension)
        }
    }
}

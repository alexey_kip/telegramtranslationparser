package ru.truebober.telegram.translation_parser

/**
 * Тип платформы
 */
enum class Platform {
    android,
    ios,
    macos,
    tdesktop
}

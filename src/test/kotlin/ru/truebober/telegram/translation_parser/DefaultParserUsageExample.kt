package ru.truebober.telegram.translation_parser

import org.junit.Test
import org.slf4j.LoggerFactory
import java.io.File
import java.util.*
import kotlin.test.assertEquals

class DefaultParserUsageExample {

    companion object {
        private val log = LoggerFactory.getLogger(DefaultParserUsageExample::class.java)
    }

    val parser = DefaultParser()

    @Test
    fun androidXmlParser() {
        //путь к файлу с переводами
        val path = this.javaClass.classLoader.getResource("android_en_v887668.xml")
        val filePath = File(path.file).toPath()

        //отдать на обработку
        val translation = parser.run(filePath)


        assertEquals(3346, translation.body.size)
        assertEquals(887668, translation.version)
        assertEquals(Locale.ENGLISH, translation.locale)
        assertEquals(Platform.android, translation.platform)

        val testKey1 = TranslationKey(Plurality.none, "AccountAlreadyLoggedIn")
        val testVal1 = "This account is already logged in from this app."
        assertEquals(testVal1, translation.body[testKey1])

        val testKey2 = TranslationKey(Plurality.none, "_tg_open_google_play")
        val testVal2 = "Open Google Play"
        assertEquals(testVal2, translation.body[testKey2])

        val testKey3 = TranslationKey(Plurality.zero, "ThemeInstallCount")
        val testVal3 = "%1\$d people are using this theme"
        assertEquals(testVal3, translation.body[testKey3])

        val testKey4 = TranslationKey(Plurality.one, "PhotosSelected")
        val testVal4 = "%1\$d photo selected"
        assertEquals(testVal4, translation.body[testKey4])

        val testKey5 = TranslationKey(Plurality.two, "ForwardedRound")
        val testVal5 = "%1\$d forwarded video messages"
        assertEquals(testVal5, translation.body[testKey5])
    }

    @Test
    fun desktopStringsParser() {
        //путь к файлу с переводами
        val path = this.javaClass.classLoader.getResource("tdesktop_en_v266037.strings")
        val filePath = File(path.file).toPath()

        //отдать на обработку
        val translation = parser.run(filePath)

        assertEquals(2576, translation.body.size)
        assertEquals(266037, translation.version)
        assertEquals(Locale.ENGLISH, translation.locale)
        assertEquals(Platform.tdesktop, translation.platform)

        val testKey1 = TranslationKey(Plurality.none, "lng_polls_quiz_results_title")
        val testVal1 = "Quiz results"
        assertEquals(testVal1, translation.body[testKey1])

        val testKey2 = TranslationKey(Plurality.other, "lng_polls_show_more")
        val testVal2 = "Show more ({count})"
        assertEquals(testVal2, translation.body[testKey2])

        val testKey3 = TranslationKey(Plurality.many, "lng_action_game_score_no_game")
        val testVal3 = "{from} scored {count}"
        assertEquals(testVal3, translation.body[testKey3])

        val testKey4 = TranslationKey(Plurality.none, "lng_settings_bg_edit_theme")
        val testVal4 = "Launch theme editor"
        assertEquals(testVal4, translation.body[testKey4])

        val testKey5 = TranslationKey(Plurality.none, "lng_admin_log_edited_caption")
        val testVal5 = "{from} edited caption:"
        assertEquals(testVal5, translation.body[testKey5])
    }

    @Test
    fun iosStringsParser() {
        //путь к файлу с переводами
        val path = this.javaClass.classLoader.getResource("ios_en_v634687.strings")
        val filePath = File(path.file).toPath()

        //отдать на обработку
        val translation = parser.run(filePath)

        val strings = translation.body.map { k -> k.key.keyName }.distinct().size
        assertEquals(3690, strings)
        assertEquals(4365, translation.body.size)
        assertEquals(634687, translation.version)
        assertEquals(Locale.ENGLISH, translation.locale)
        assertEquals(Platform.ios, translation.platform)

        val testKey1 = TranslationKey(Plurality.none, "Updated.AtDate")
        val testVal1 = "%@"
        assertEquals(testVal1, translation.body[testKey1])

        val testKey2 = TranslationKey(Plurality.few, "Updated.HoursAgo")
        val testVal2 = "%@ hours ago"
        assertEquals(testVal2, translation.body[testKey2])

        val testKey3 = TranslationKey(Plurality.none, "SettingsSearch.Synonyms.ChatSettings.OpenLinksIn")
        val testVal3 = "Browser"
        assertEquals(testVal3, translation.body[testKey3])

        val testKey4 = TranslationKey(Plurality.none, "Appearance.ThemePreview.ChatList.1.Name")
        val testVal4 = "Alicia Torreaux"
        assertEquals(testVal4, translation.body[testKey4])

        val testKey5 = TranslationKey(Plurality.many, "Notifications.ExceptionMuteExpires.Minutes")
        val testVal5 = "In %@ minutes"
        assertEquals(testVal5, translation.body[testKey5])
    }

    @Test
    fun macosStringsParser() {
        //путь к файлу с переводами
        val path = this.javaClass.classLoader.getResource("macos_en_v142427.strings")
        val filePath = File(path.file).toPath()

        //отдать на обработку
        val translation = parser.run(filePath)

        val strings = translation.body.map { k -> k.key.keyName }.distinct().size
        assertEquals(2313, strings)
        assertEquals(2613, translation.body.size)
        assertEquals(142427, translation.version)
        assertEquals(Locale.ENGLISH, translation.locale)
        assertEquals(Platform.macos, translation.platform)

        val testKey1 = TranslationKey(Plurality.none, "_NS:103.title")
        val testVal1 = "Edit"
        assertEquals(testVal1, translation.body[testKey1])

        val testKey2 = TranslationKey(Plurality.none, "ChatList.Mute.1Hour")
        val testVal2 = "For 1 Hour"
        assertEquals(testVal2, translation.body[testKey2])

        val testKey3 = TranslationKey(Plurality.one, "Peer.Status.Member.Online")
        val testVal3 = "%d online"
        assertEquals(testVal3, translation.body[testKey3])

        val testKey4 = TranslationKey(Plurality.none, "Chat.Service.Channel.UpdatedPhoto")
        val testVal4 = "channel photo updated"
        assertEquals(testVal4, translation.body[testKey4])

        val testKey5 = TranslationKey(Plurality.none, "LE2-aR-0XJ.title")
        val testVal5 = "Bring All to Front"
        assertEquals(testVal5, translation.body[testKey5])
    }
}
